// Almost as pure as Vannilla JS 

var myNum = 0;

$(document).ready(function(){
    
    var originOpa = 1;

    var target = document.getElementsByClassName('model')[0];

    var guide = document.getElementsByClassName('guide')[0];

    target.style.opacity = 0;

    document.addEventListener("keydown", function(e){
        if(e.altKey && e.shiftKey && e.code === "KeyA"){

            myNum = Math.round(++myNum);
            console.log(myNum)

            if(myNum % 2 !== 0){
                target.style.opacity = 1;
                myNum = 1;
            } else {
                target.style.opacity = 0;
                myNum = 0;
            }

        };

        if (e.altKey && e.shiftKey && e.code === "KeyS"){

            myNum = Math.round((myNum - 0.1)*10) / 10;

            if (myNum >= 0){
                target.style.opacity = myNum;
                console.log(myNum);
            } else if (myNum < 0) {
                myNum = 0;
                console.log(myNum);
                return false;
            }
                
        };

        if (e.altKey && e.shiftKey && e.code === "KeyD"){

            myNum = Math.round((myNum + 0.1)*10) / 10;


            if(myNum <= 1) {
                target.style.opacity = myNum;
                console.log(myNum);


            } else if (myNum > 1){
                myNum = 1;
                console.log(myNum);
                return false;
            }
        }

        if (e.altKey && e.shiftKey && e.code === "KeyW"){

            var localNum2 = ++myNum;
            console.log(localNum2)

            if(localNum2 % 2 !== 0){
                guide.style.opacity = 0;
            } else {
                guide.style.opacity = 1;
            }

        }
    });
    
})

